package ru.mtumanov.tm.api.controller;

import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.exception.field.AbstractFieldException;

public interface ITaskController {

    void createTask() throws AbstractFieldException;

    void showTasks();

    void clearTasks();

    void showTaskById() throws AbstractFieldException;

    void showTasktByIndex() throws AbstractFieldException;

    void showTaskByProjectId();

    void removeTaskById() throws AbstractFieldException, AbstractEntityNotFoundException;

    void removeTaskByIndex() throws AbstractFieldException, AbstractEntityNotFoundException;

    void updateTaskById() throws AbstractFieldException, AbstractEntityNotFoundException;

    void updateTaskByIndex() throws AbstractFieldException, AbstractEntityNotFoundException;

    void startTaskByIndex() throws AbstractFieldException, AbstractEntityNotFoundException;

    void startTaskById() throws AbstractFieldException, AbstractEntityNotFoundException;

    void completeTaskByIndex() throws AbstractFieldException, AbstractEntityNotFoundException;

    void completeTaskById() throws AbstractFieldException, AbstractEntityNotFoundException;

    void changeTaskStatusByIndex() throws AbstractFieldException, AbstractEntityNotFoundException;

    void changeTaskStatusById() throws AbstractFieldException, AbstractEntityNotFoundException;

}
