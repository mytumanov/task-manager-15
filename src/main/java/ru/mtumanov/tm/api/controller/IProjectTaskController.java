package ru.mtumanov.tm.api.controller;

import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.exception.field.AbstractFieldException;

public interface IProjectTaskController {

    void bindTaskToProject() throws AbstractFieldException, AbstractEntityNotFoundException;

    void unbindTaskFromProject() throws AbstractFieldException, AbstractEntityNotFoundException;
}
