package ru.mtumanov.tm.api.controller;

public interface ICommandController {

    void showInfo();

    void exit();

    void showArguemntError();

    void showCommandError();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

}
