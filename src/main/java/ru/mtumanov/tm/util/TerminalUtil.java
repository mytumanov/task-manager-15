package ru.mtumanov.tm.util;

import java.util.Scanner;

import ru.mtumanov.tm.exception.field.NumberIncorrectException;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() throws NumberIncorrectException {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (final Exception e) {
            throw new NumberIncorrectException(value);
        }
    }

}
