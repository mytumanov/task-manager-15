package ru.mtumanov.tm.exception;

public abstract class AbstractException extends Exception {

    public AbstractException() {
    }

    public AbstractException(final String message) {
        super(message);
    }

    public AbstractException(final String messgae, final Throwable cause) {
        super(messgae, cause);
    }

    public AbstractException(final Throwable cause) {
        super(cause);
    }

    public AbstractException(
            final String message, final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
