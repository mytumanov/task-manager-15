package ru.mtumanov.tm.exception.system;

import ru.mtumanov.tm.exception.AbstractException;

public abstract class AbstractSytemException extends AbstractException {

    public AbstractSytemException() {
    }

    public AbstractSytemException(final String message) {
        super(message);
    }

    public AbstractSytemException(final String messgae, final Throwable cause) {
        super(messgae, cause);
    }

    public AbstractSytemException(final Throwable cause) {
        super(cause);
    }

    public AbstractSytemException(
            final String message, final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
