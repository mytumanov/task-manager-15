package ru.mtumanov.tm.exception.system;

public final class CommandNotSupportedException extends AbstractSytemException {

    public CommandNotSupportedException() {
        super("ERROR! Command not supported!");
    }

    public CommandNotSupportedException(final String cmd) {
        super("ERROR! Command: " + cmd + " not supported!");
    }

}
