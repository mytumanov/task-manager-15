package ru.mtumanov.tm.repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.exception.entity.ProjectNotFoundException;
import ru.mtumanov.tm.model.Project;

public final class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final Project project) {
        projects.add(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public Project findOneById(final String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public void remove(final Project project) {
        projects.remove(project);
    }

    @Override
    public Project removeById(final String id) throws AbstractEntityNotFoundException {
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) throws AbstractEntityNotFoundException {
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        remove(project);
        return project;
    }

    @Override
    public int getSize() {
        return projects.size();
    }

    @Override
    public boolean existById(final String id) {
        return findOneById(id) != null;
    }

    @Override
    public List<Project> findAll(final Comparator comparator) {
        final List<Project> result = new ArrayList<>(projects);
        result.sort(comparator);
        return result;
    }

}
