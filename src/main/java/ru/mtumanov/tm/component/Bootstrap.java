package ru.mtumanov.tm.component;

import ru.mtumanov.tm.api.controller.ICommandController;
import ru.mtumanov.tm.api.controller.IProjectController;
import ru.mtumanov.tm.api.controller.IProjectTaskController;
import ru.mtumanov.tm.api.controller.ITaskController;
import ru.mtumanov.tm.api.repository.ICommandRepository;
import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.api.service.ICommandService;
import ru.mtumanov.tm.api.service.IProjectService;
import ru.mtumanov.tm.api.service.IProjectTaskService;
import ru.mtumanov.tm.api.service.ITaskService;
import ru.mtumanov.tm.constant.ArgumentConstant;
import ru.mtumanov.tm.constant.CommandConstant;
import ru.mtumanov.tm.controller.CommandController;
import ru.mtumanov.tm.controller.ProjectController;
import ru.mtumanov.tm.controller.ProjectTaskController;
import ru.mtumanov.tm.controller.TaskController;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.exception.system.ArgumentNotSupportedException;
import ru.mtumanov.tm.exception.system.CommandNotSupportedException;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.model.Task;
import ru.mtumanov.tm.repository.CommandRepository;
import ru.mtumanov.tm.repository.ProjectRepository;
import ru.mtumanov.tm.repository.TaskRepository;
import ru.mtumanov.tm.service.CommandService;
import ru.mtumanov.tm.service.ProjectService;
import ru.mtumanov.tm.service.ProjectTaskService;
import ru.mtumanov.tm.service.TaskService;
import ru.mtumanov.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    public void start(final String[] args) {
        initDemo();
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");
        while (true) {
            System.out.println("ENTER COMMAND:");
            try {
                processCommand(TerminalUtil.nextLine());
                System.out.println("OK");
            } catch (final Exception e) {
                System.out.println(e.getMessage());
                System.out.println("FAIL");
            }
        }
    }

    private void initDemo() {
        try {
            projectService.add(new Project("Test Project", "Project for test", Status.IN_PROGRESS));
            projectService.add(new Project("Complete Project", "Project with complete status", Status.COMPLETED));
            projectService.add(new Project("FKSKDJ#&$&*@(!!111", "Strange project", Status.NOT_STARTED));
            projectService.add(new Project("11112222", "Project with numbers", Status.IN_PROGRESS));

            taskService.add(new Task("NAME1", "description for task"));
            taskService.add(new Task("NAME2", "description2"));
            taskService.add(new Task("NAME3", "description3"));
        } catch (final AbstractEntityNotFoundException e) {
            System.out.println("ERROR! Fail to nitialized test data.");
            System.out.println(e.getMessage());
        }
    }

    private void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        try {
            processArgument(args[0]);
            System.exit(0);
        } catch (final ArgumentNotSupportedException e) {
            System.out.println(e.getMessage());
        }
    }

    private void processArgument(final String arg) throws ArgumentNotSupportedException {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConstant.CMD_ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConstant.CMD_HELP:
                commandController.showHelp();
                break;
            case ArgumentConstant.CMD_VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.CMD_INFO:
                commandController.showInfo();
                break;
            case ArgumentConstant.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConstant.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                throw new ArgumentNotSupportedException(arg);
        }
    }

    private void processCommand(final String arg) throws AbstractException {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case CommandConstant.CMD_ABOUT:
                commandController.showAbout();
                break;
            case CommandConstant.CMD_HELP:
                commandController.showHelp();
                break;
            case CommandConstant.CMD_VERSION:
                commandController.showVersion();
                break;
            case CommandConstant.CMD_INFO:
                commandController.showInfo();
                break;
            case CommandConstant.CMD_EXIT:
                commandController.exit();
                break;
            case CommandConstant.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case CommandConstant.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case CommandConstant.CMD_PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConstant.CMD_PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConstant.CMD_PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConstant.CMD_TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConstant.CMD_TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CommandConstant.CMD_TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConstant.CMD_PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case CommandConstant.CMD_PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case CommandConstant.CMD_PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CommandConstant.CMD_PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CommandConstant.CMD_PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case CommandConstant.CMD_PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case CommandConstant.CMD_TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case CommandConstant.CMD_TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case CommandConstant.CMD_TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case CommandConstant.CMD_TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case CommandConstant.CMD_TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case CommandConstant.CMD_TASK_SHOW_BY_INDEX:
                taskController.showTasktByIndex();
                break;
            case CommandConstant.CMD_PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case CommandConstant.CMD_PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case CommandConstant.CMD_PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case CommandConstant.CMD_PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case CommandConstant.CMD_PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case CommandConstant.CMD_PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case CommandConstant.CMD_TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case CommandConstant.CMD_TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case CommandConstant.CMD_TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case CommandConstant.CMD_TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case CommandConstant.CMD_TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case CommandConstant.CMD_TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case CommandConstant.CMD_TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case CommandConstant.CMD_TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case CommandConstant.CMD_TASK_SHOW_BY_PROJECT_ID:
                taskController.showTaskByProjectId();
                break;
            default:
                throw new CommandNotSupportedException(arg);
        }
    }

}
