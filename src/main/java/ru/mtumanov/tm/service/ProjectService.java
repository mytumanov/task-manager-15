package ru.mtumanov.tm.service;

import java.util.Comparator;
import java.util.List;

import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.api.service.IProjectService;
import ru.mtumanov.tm.enumerated.Sort;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.exception.entity.ProjectNotFoundException;
import ru.mtumanov.tm.exception.field.AbstractFieldException;
import ru.mtumanov.tm.exception.field.DescriptionEmptyException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.IndexIncorectException;
import ru.mtumanov.tm.exception.field.NameEmptyException;
import ru.mtumanov.tm.model.Project;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project create(final String name, final String description) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(project);
        return project;
    }

    @Override
    public void add(final Project project) throws AbstractEntityNotFoundException {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.add(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findOneById(final String id) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.findOneById(id);
    }

    @Override
    public Project findOneByIndex(final Integer index) throws AbstractFieldException {
        if (index == null || index < 0) throw new IndexIncorectException();
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public void remove(final Project project) throws AbstractEntityNotFoundException {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.remove(project);
    }

    @Override
    public Project removeById(final String id) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeByIndex(final Integer index) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (index == null || index < 0) throw new IndexIncorectException();
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project updateById(final String id, final String name, final String description) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (index == null || index < 0) throw new IndexIncorectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (index == null || index < 0) throw new IndexIncorectException();
        if (index > projectRepository.getSize()) throw new IndexOutOfBoundsException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public List<Project> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return projectRepository.findAll(comparator);
    }

    @Override
    public List<Project> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

}
